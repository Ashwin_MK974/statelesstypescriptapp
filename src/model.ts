export interface MessageInt{
    id:number,
    message:String | undefined,
    date:number
}